include config.mk

RELEASES =
include releases.mk

.PHONY: ci mkdir-public

ci: mkdir-public
	for release in $(RELEASES) ; do $(CP) $$release public ; done
	$(CP) $$(for release in $(RELEASES) ; do echo $$release ; done | sort -V | tail -n 1) public/tarot-latest.tar.gz
	./deb-mr
	./github-pr
	(cd public && $(TREE_H) . > index.html)
	$(CP) submit public/

mkdir-public:
	$(RM_RF) public
	$(MKDIR_P) public
